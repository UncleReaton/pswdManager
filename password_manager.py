import database_controller as dbControl
import empty_clipboard as ec

print("Welcome to HM Password Manager 1.0")

exitValue = False

if dbControl.master_password() == True:

	while(exitValue != True):

		

			print("           ————————Menu—————————")
			print("1. Generate a new password")
			print("2. Find a password for a specific service")
			print("3. Find a password for a specific email address")
			print("4. Copy a password from a specific id")
			print("5. Clear the clipboard")
			print("Q. Exit")
			print("")

			menuChoice = str(input())

			if menuChoice == "1":
				dbControl.insert_password()
			elif menuChoice == "2":
				dbControl.find_service()
			elif menuChoice == "3":
				dbControl.find_email()
			elif menuChoice == "4":
				dbControl.copy_by_id()
			elif menuChoice == "5":
				ec.empty()
			elif menuChoice.casefold() == "q":
				exitValue = True
				print("See you next time!")
			else:
				print("Error")
				print("Type 1, 2, 3, 4, 5 or Q")
else:
	print("Get out !")