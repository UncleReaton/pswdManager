#Import the libraries we're going to use
import random #to create a random password
import string #to give options to the password generator
import pyperclip as pc #to copy password and emptying the clipboard

def account_generator():
	def password_generator(str_size, allowed_chars):
		return "".join(random.SystemRandom().choice(allowed_chars) for x in range(str_size))

	#Password Options
	print("How long do you want your password to be? (Max 30) : ")
	size = 0
	while size == 0 or size > 30:
		size = int(input())
	chars = string.ascii_letters + string.punctuation + string.digits
	

	def id_generator(str_size, allowed_chars):
		return "".join(random.SystemRandom().choice(allowed_chars) for x in range(str_size))

	#ID Options
	idSize = 4
	idChars = string.ascii_letters + string.digits

	#Completing info
	service_id = id_generator(idSize, idChars)
	password = password_generator(size, chars)
	print("enter the name of the service : ")
	app_name = input()
	print("enter your email address : ")
	email = input()
	print("enter your username : ")
	username = input()
	print("enter the url of the service : ")
	url = input()

	pc.copy(password)

	#Create a python dict
	account = {
		"_id": service_id,
		"password": password,
		"email": email,
		"username": username,
		"url": url,
		"app_name": app_name
	}
	return account