#Import the libraries we're going to use
import pymongo #Connect to mongo database
import account_creator as ac
import pyperclip as pc #to copy password and emptying the clipboard
import bcrypt #to compare the hash of the password
import getpass #to hide the user input

#Connecting to the database
client = pymongo.MongoClient("mongodb://localhost:27017/")
database = client["password_manager"]
collection = database["accounts"]


def insert_password():
	add = ac.account_generator() #use the function account_generator from account_creator.py
	insert = collection.insert_one(add) #add the return of account_generator to our collection

def find_email():
	print("What email are you looking for :")
	print("")
	email = str(input())
	print("")
	find = collection.find({"email": email})
	print("Here is all the accounts for " + email)
	for row in find:
		print("————————————————————————————————————————————————")
		service_id = str(row["_id"])
		print("[id : " + service_id + " ]")
		password = row["password"]
		print("password : " + password)
		username = row["username"]
		print("username : " + username)
		url = row["url"]
		print("URL : " + url)
		app_name = row["app_name"]
		print("service : " + app_name)
		print("————————————————————————————————————————————————")

def find_service():
	print("What service are you looking for :")
	print("")
	service = str(input())
	print("")
	find = collection.find({"app_name": service})
	print("Here is all the accounts for " + service)
	print("")
	for row in find:
		print("————————————————————————————————————————————————")
		service_id = str(row["_id"])
		print("[id : " + service_id + " ]")
		password = row["password"]
		print("password : " + password)
		username = row["username"]
		print("username : " + username)
		email = row["email"]
		print("email : " + email)
		url = row["url"]
		print("URL : " + url)
		print("————————————————————————————————————————————————")

def copy_by_id():
	print("What is the id of the account researched ?")
	print("")
	srch_id = str(input())
	print("")
	find = collection.find({"_id": srch_id})
	print("Here is the informations linked to this id :")
	print("")
	for row in find:
		print("————————————————————————————————————————————————")
		service_id = str(row["_id"])
		print("[id : " + service_id + " ]")
		password = row["password"]
		print("password : " + password)
		pc.copy(password)
		username = row["username"]
		print("username : " + username)
		email = row["email"]
		print("email : " + email)
		url = row["url"]
		print("URL : " + url)
		app_name = row["app_name"]
		print("service : " + app_name)				
		print("————————————————————————————————————————————————")
	print("The password has been copied into your clipboard !")

def master_password():
	client = pymongo.MongoClient("mongodb://localhost:27017/")
	database = client["masterpassword"]
	collection = database["master"]

	print("What is your master password ?")
	print("")
	password = getpass.getpass().encode("utf-8")

	find = collection.find({"get": "password"})
	for row in find:
		masterPswdHashed = row["master"].encode("utf-8")
		if bcrypt.checkpw(password, masterPswdHashed):
			print("It matches !")
			return True
		else:
			return False